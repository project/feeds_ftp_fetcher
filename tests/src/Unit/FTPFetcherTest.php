<?php

declare(strict_types=1);

namespace Drupal\Tests\feeds_ftp_fetcher\Unit;

use Drupal\feeds\Entity\FeedType;
use Drupal\feeds_ftp_fetcher\Adapter\FtpConnectionAdapter;
use Drupal\feeds_ftp_fetcher\Feeds\Fetcher\FtpFetcher;
use Drupal\key\KeyRepositoryInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the FTPFetcher class.
 *
 * @coversDefaultClass \Drupal\feeds_ftp_fetcher\Feeds\Fetcher\FtpFetcher
 * @group feeds_ftp_fetcher
 */
class FTPFetcherTest extends UnitTestCase {

  /**
   * The mocked FtpConnectionAdapter.
   */
  protected FtpConnectionAdapter|MockObject $ftpConnectionAdapter;

  /**
   * The mocked KeyRepositoryInterface.
   */
  protected KeyRepositoryInterface|MockObject $keyRepository;

  /**
   * The FtpFetcher under test.
   */
  protected FtpFetcher $ftpFetcher;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->keyRepository = $this->createMock(KeyRepositoryInterface::class);
    $this->ftpConnectionAdapter = $this->createMock(FtpConnectionAdapter::class);

    $this->ftpFetcher = new FtpFetcher(
      configuration: ['feed_type' => $this->createMock(FeedType::class)],
      plugin_id: 'ftp_fetcher',
      plugin_definition: [],
      keyRepository: $this->keyRepository,
      ftpConnectionAdapter: $this->ftpConnectionAdapter
    );
  }

  /**
   * Tests the fetchFromFtp() method.
   */
  public function testFetchFromFtp() {
    $config = [
      'host' => 'ftp.example.com',
      'port' => 21,
      'username' => 'user',
      'password' => 'password',
      'file_path' => '/path/to/file.csv',
    ];

    $this->ftpConnectionAdapter->expects($this->once())
      ->method('fetchFromFtp')
      ->with($config)
      ->willReturn('Sample file content');

    $result = $this->ftpFetcher->fetchFromFtp($config);
    $this->assertEquals('Sample file content', $result);
  }

}
