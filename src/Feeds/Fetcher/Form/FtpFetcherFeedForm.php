<?php

declare(strict_types = 1);

namespace Drupal\feeds_ftp_fetcher\Feeds\Fetcher\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Drupal\key\KeyRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form on the feed edit page for the FtpFetcher.
 */
class FtpFetcherFeedForm extends ExternalPluginFormBase implements ContainerInjectionInterface {

  /**
   * Constructs an FTP Fetcher form object.
   *
   * @param \Drupal\key\KeyRepository $keyRepository
   *   The key repository.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   */
  public function __construct(
    protected readonly KeyRepository $keyRepository,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly LoggerInterface $logger,
    protected readonly Messenger $messenger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('key.repository'),
      $container->get('file_system'),
      $container->get('logger.channel.feeds_ftp_fetcher'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, ?FeedInterface $feed = NULL): ?array {
    if (!$feed) {
      return NULL;
    }

    $config = $feed->getConfigurationFor($this->plugin);

    $form['host'] = [
      '#title' => $this->t('Host'),
      '#type' => 'textfield',
      '#default_value' => $feed->getSource(),
      '#description' => $this->t('The domain of the FTP server.'),
      '#maxlength' => 2048,
      '#required' => TRUE,
    ];

    $form['port'] = [
      '#title' => $this->t('Port'),
      '#type' => 'number',
      '#default_value' => $config['port'] ?? 21,
      '#description' => $this->t('The port of the FTP server.'),
      '#maxlength' => 2048,
      '#required' => TRUE,
    ];

    $form['username'] = [
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
      '#default_value' => $config['username'] ?? NULL,
      '#description' => $this->t('The FTP username.'),
      '#maxlength' => 2048,
      '#required' => TRUE,
    ];

    $keys = $this->keyRepository->getKeys();
    $options = [];
    foreach ($keys as $key => $value) {
      $options[$key] = $value->label();
    }
    $form['password'] = [
      '#title' => $this->t('Password'),
      '#type' => 'select',
      '#default_value' => NULL,
      '#description' => $this->t('The FTP password provided by the Key module.'),
      '#options' => $options,
    ];

    $form['file_path'] = [
      '#title' => $this->t('File Path'),
      '#type' => 'textfield',
      '#default_value' => $config['file_path'] ?? NULL,
      '#maxlength' => 2048,
      '#description' => $this->t('Path to file starting from FTP root with a leading /.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ?FeedInterface $feed = NULL): void {
    if (!$feed) {
      return;
    }
    $feed->setSource($form_state->getValue('host'));
    $config = $form_state->getValues();
    // Convert port to integer.
    if (isset($config['port']) && !is_int($config['port'])) {
      $config['port'] = intval($config['port']);
    }
    $feed->setConfigurationFor($this->plugin, $config);
  }

}
