<?php

declare(strict_types=1);

namespace Drupal\feeds_ftp_fetcher\Feeds\Fetcher;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\RawFetcherResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds_ftp_fetcher\Adapter\FtpConnectionAdapter;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an FTP Fetcher.
 *
 * @FeedsFetcher(
 *   id = "ftp_fetcher",
 *   title = @Translation("FTP fetcher"),
 *   description = @Translation("Fetch file from FTP"),
 *   form = {
 *     "feed" =
 *   "Drupal\feeds_ftp_fetcher\Feeds\Fetcher\Form\FtpFetcherFeedForm",
 *   }
 * )
 */
class FtpFetcher extends PluginBase implements FetcherInterface, ContainerFactoryPluginInterface {

  /**
   * The FtpFetcher constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   The key repository.
   * @param \Drupal\feeds_ftp_fetcher\Adapter\FtpConnectionAdapter $ftpConnectionAdapter
   *   The FTP connection adapter.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected readonly KeyRepositoryInterface $keyRepository,
    protected readonly FtpConnectionAdapter $ftpConnectionAdapter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('key.repository'),
      $container->get('feeds_ftp_fetcher.ftp_adapter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $config = $feed->getConfigurationFor($this);
    $result = $this->fetchFromFtp($config);
    return new RawFetcherResult($result);
  }

  /**
   * Create the FTP Connection.
   *
   * @param array $config
   *   Drupal Config object.
   *
   * @return string|null
   *   Returns an JSON encoded array of stdClass objects.
   */
  public function fetchFromFtp(array $config): ?string {
    return $this->ftpConnectionAdapter->fetchFromFtp($config);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration(): array {
    return [
      'host' => '',
      'port' => 0,
      'username' => '',
      'password' => '',
      'file_path' => '',
    ];
  }

}
