<?php

namespace Drupal\feeds_ftp_fetcher\Adapter;

use Drupal\key\KeyRepositoryInterface;
use FTP\Connection;

/**
 * FTP connection adapter.
 */
class FtpConnectionAdapter {

  /**
   * FtpConnectionAdapter constructor.
   *
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   The key repository.
   */
  public function __construct(
    protected KeyRepositoryInterface $keyRepository
  ) {}

  /**
   * Fetch the data from the FTP server.
   *
   * @param array $config
   *   Drupal config object.
   *
   * @return string|null
   *   Returns an JSON encoded array of stdClass objects.
   */
  public function fetchFromFtp(array $config): ?string {
    $ftp_connection = $this->getFtpConnection($config);
    $file_path = $config['file_path'];
    $handle = tmpfile();
    ftp_fget($ftp_connection, $handle, $file_path, FTP_ASCII);
    rewind($handle);
    $file_content = stream_get_contents($handle);
    ftp_close($ftp_connection);
    return $file_content;
  }

  /**
   * Create the FTP Connection.
   *
   * @return \FTP\Connection|Null
   *   The remote filesystem object or null if a connection cannot be
   *   established.
   */
  public function getFtpConnection(array $config): ?Connection {
    $password = $this->keyRepository
      ->getKey($config['password'])
      ->getKeyValue();
    /** @var \FTP\Connection|FALSE $ftp_connection  */
    $ftp_connection = ftp_connect($config['host'], $config['port']);
    if (!$ftp_connection) {
      return NULL;
    }
    ftp_pasv($ftp_connection, TRUE);
    ftp_login($ftp_connection, $config['username'], $password);
    return $ftp_connection;
  }

}
